package com.example.basicactfragapp.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.basicactfragapp.R

class FragmentTwo : Fragment() {

    companion object {
        fun newInstance() = FragmentTwo()
    }

    private lateinit var viewModel: FragmentTwoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val nextButton = view.findViewById(R.id.nextFragmentButton) as (Button)
        nextButton.visibility = Button.INVISIBLE

        val homeButton = view.findViewById(R.id.mainFragmentButton) as (Button)
        homeButton.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentTwo_to_mainFragment)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FragmentTwoViewModel::class.java)
        // TODO: Use the ViewModel
    }

}